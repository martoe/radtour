5-tägige Radtour (Sommer)

# Bekleidung
* 4 T-Shirts
* 2 Paar Socken
* Pullover
* Kurze Hose
* 4 Unterhosen
* Regenjacke

# Toilette
* Zahnbürste
* Zahnpasta
* Seife/Duschgel
* Taschentücher
* Klopapier
* Vasiline

# Baden
* Badehose
* Handtuch
* Sonnenbrille
* Sonnencreme

# Fahrrad
* Helm
* Handschuhe
* Werkzeug
* Flickzeug
* Ersatzschlauch
* Trinkflasche
* Beleuchtung
* Schloss
* Straßenkarte
* Pumpe

# Misc
* Handy-Ladegerät/Powerbank
* Erste-Hilfe-Set
* Asthma-Spray
* Taschenmesser
* Feuchttücher
* Müsliriegel
* Notizheft + Stift