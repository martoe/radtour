# 1. Etappe (Di, 7.8.2018)

* mit dem Zug von Wien nach Salzburg
* [Salzburg - Golling](https://goo.gl/maps/mYWHwtEs1Tz) (31 km, 180 hm) - [GPX](./etappe1-salzburg-golling.gpx)
* Fahrzeit (exkl. große Pause): 2:20 Std
* Übernachtung: [Gästehaus Sunkler](https://pension-golling.at/)

# 2. Etappe (Mi, 8.8.2018)

* [Golling - St. Veit/Pongau](https://goo.gl/maps/7QDwtnoT8es) (48 km, 580 hm) - [GPX](./etappe2-golling-stveit.gpx)
* Fahrzeit (exkl. große Pause): 4 Std
* Übernachtung: [Hotel Posauner](https://www.posauner.com/)

# 3. Etappe (Do, 9.8.2018)

* [St. Veit/Pongau - Bad Gastein](https://goo.gl/maps/payd1nMJ1bQ2) (28 km, 520 hm) - [GPX](./etappe3-stveit-badgastein.gpx)
* Fahrzeit (exkl. große Pause): 3 Std
* Übernachtung: [Pension Steinbacher](https://www.pension-steinbacher.at/)

# 4. Etappe (Fr, 10.8.2018)

* [Bad Gastein - Tauernschleuse](https://goo.gl/maps/9s6onkZUGfk) (4 km, 100 hm) - [GPX](./etappe4a-badgastein-tauernschleuse.gpx)
* mit dem Zug durch die [Tauernschleuse](https://www.oebb.at/de/angebote-ermaessigungen/autoschleuse-tauernbahn)
* [Tauernschleuse - Millstätter See](https://goo.gl/maps/pYc12MqNHHw) (44 km, 260 hm) - [GPX](./etappe4b-tauernschleuse-millstaettersee.gpx)
* Fahrzeit (exkl. große Pause): 4 Std
* Übernachtung: [Pension Hrnjic](https://www.millstaettersee.com/de/seeboden-am-millstaetter-see/unterkuenfte/details/pension/pension-hrnjic.html)

# 5. Etappe (Sa, 11.8.2018)

* [Millstätter See - Keutschacher See](https://goo.gl/maps/5KeHdKudaoo) (76 km, 350 hm) - [GPX](./etappe5-millstaettersee-keutschachersee.gpx)
* Ziel: [Pension Vogtland](https://www.pensionvogtland.at/)
* Vorzeitiges Ende in Villach (45 km, 110 hm, 3 Std)

----

GPS-Tracks basierend auf den [GPX-Files](https://tracks.world/?dir=at/trk38sg969) aus dem Buch [Bikeline Alpe Adria Radweg](https://www.esterbauer.com/db_detail.php?buecher_code=CAAR), bearbeitet mit [ape@map](http://apemap.com/)
